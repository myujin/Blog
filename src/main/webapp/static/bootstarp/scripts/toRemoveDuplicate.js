$(function() {
	loadGrade();
	loadDiscpline();
	$('#query1').click(selectQuestionOfCount);
	$('#receive1').click(receive1);
	$('#query2').click(function() {
		selectQuestionByName(1);
	});
	$('.receive2').click(receive2);
	pagingInit();
})

function loadGrade() {
	$.ajax({
		url : '/properties/getGrade.json',
		dataType: 'json',
		success : function(data) {
//			var stages = eval('('+data+')');
//			alert(data.length);
			$('#stage').html('');
			$('#stage').append('<option>全部</option>')
			for(var stage in data) {
				$('#stage').append('<option>'+data[stage]+'</option>')
			}
		}
	})
}

function loadDiscpline() {
	$.ajax({
		url : '/properties/getDiscpline.json',
		dataType: 'json',
		success : function(data) {
			$('#discpline').html('');
			$('#discpline').append('<option value="0">全部</option>')
			for(var discpline in data) {
				$('#discpline').append('<option value='+data[discpline].id+'>'+data[discpline].name+'</option>')
			}
		}
	})
}

function selectQuestionOfCount() {
	$.ajax({
		url : '/6/questions/getQuestionsCount',
		type : 'post',
		data : {
			grade : $('#stage option:selected').text(),
			discpline : $('#discpline option:selected').val()
		},
		success : function(data) {
			$('#resultCount').val(data);
		}
	})
}

function receive1() {
	if($('#resultCount').val() == '') {
		alert('请先点击查询，再领取');
		return;
	}
	if($('#receiveCount').val() == '') {
		alert('请输入要领取的试题数量');
		return;
	}
	if(!checkQnumber($('#receiveCount'))){
		alert('请正确输入数字');
		return;
	}
	$.ajax({
		url : '/task/receive1',
		type : 'post',
		data : {
			stage : $('#stage option:selected').text(),
			discpline : $('#discpline option:selected').val(),
			count : $('#receiveCount').val(),
		},
		dataType : 'json',
		success : function(data) {
			alert(data.message);
			$('#resultCount').val('');
			$('#receiveCount').val('');
		}
	})
}

function selectQuestionByName(page) {
//	alert(event.data.page)
//	var page = parseInt(event.data.page);
	page = parseInt(page);
	if(page > parseInt($('#totalPage').text()))
		page = parseInt($('#totalPage').text());
	if(page == 0)
		page =1;
	$.ajax({
		url : '/6/questions/getQuestionByTestpaper',
		type : 'POST',
		dataType : 'json',
		data : {
			cata : 'testpaper',
			queryStr : $('#queryStr').val(),
			page : page
		},
		success : function(data) {
			var tbody = $('#tbody');
			tbody.html('');
			var str = '';
			var resultList = data.data;
			for(var i in resultList) {
				str +='<tr>';
				str +='<td style="display:none">'+resultList[i].id+'</td>'
				str +='<td><input type="checkbox" /></td>';
				str +='<td>'+resultList[i].name+'</td>';
				str +='<td>'+resultList[i].total+'</td>';
				str +='<td>'+resultList[i].receivableCount+'</td>';
				str +='</tr>';
			}
			tbody.append(str);
			$('#thisPage').text(data.curPage);
			$('#totalPage').text(data.total);
			total = data.total;
		}
	})
}

function receive2() {
	var ids = [];
	$('#tbody tr td input[type=checkbox]:checked').each(function() {
//		alert($(this).parent().prev('td').text());
		ids.push($(this).parent().prev('td').text());
	});
	if(ids.length == 0) {
		alert("您未选择试卷，请至少选择一套试卷！");
		return;
	}
	$.ajax({
		url : '/task/receive2',
		type : 'post',
		data : {
			ids : ids
		},
		dataType : 'json',
		success : function(data) {
			alert(data.message);
			selectQuestionByName($('#thisPage').text());
		}
	})
}

function pagingInit() {
	$('#firstPage').click(function(){
		selectQuestionByName(1);
	});
	$('#lastPage').click(function() {
		selectQuestionByName($('#totalPage').text());
	});
	$('#nextPage').click(function() {
		selectQuestionByName(parseInt($('#thisPage').text())+1);
	});
	$('#prePage').click(function() {
		selectQuestionByName(parseInt($('#thisPage').text())-1);
	});
}

function checkQnumber(obj) {
	if(/^[1-9]+[0]*$/.test(obj.val())) {
		return true;
	}
	else {
		return false;
	}
}

