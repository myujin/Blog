<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/static/bootstarp/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="${pageContext.request.contextPath}/static/bootstarp/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/static/bootstarp/dist/css/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="${pageContext.request.contextPath}/static/bootstarp/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Bootstrap Core JavaScript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstarp/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstarp/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Morris Charts JavaScript
<script type="text/javascript" src="${pageContext.request.contextPath}/static/admin/bootstarp/bower_components/raphael/raphael-min.js"></script> -->
<!-- 
<script type="text/javascript" src="${pageContext.request.contextPath}/static/admin/bootstarp/bower_components/morrisjs/morris.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/admin/bootstarp/js/morris-data.js"></script> 
-->
<!-- Custom Theme JavaScript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstarp/dist/js/sb-admin-2.js"></script>
<!-- 让IE9以下版本支持html5 -->
<!--[if lt IE 9]>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstarp/scripts/html5shiv.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstarp/scripts/respond.min.js"></script>
<![endif]-->