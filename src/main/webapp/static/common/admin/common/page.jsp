<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<form id="pager" action="" method="get">
<input id="currentPage" type="hidden" name="currentPage" value="<%=SystemPager.getCurrentpage() %>"/>
<nav>
  <div class="text-muted mart20">第<strong id="thisPage">${page.currentPage}</strong>页 / 共<strong id="totalPage">${page.totalPage}</strong>页</div>
  <ul class="pagination">
  
	<li>
      <a href="javascript:goPage(1);" aria-label="Previous">
        <span aria-hidden="true">首页</span>
      </a>
    </li>
    
    <c:if test="${page.totalPage != 0 && page.totalPage != 1}"><!-- 只有一页 -->
    <li>
      <a href="javascript:prePage(${page.currentPage-1});" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    </c:if>
    
    <c:forTokens items="${page.pageNoDisp}" delims="|" var="pNo">
     <c:choose>  
      <c:when test="${pNo == 0}">  
        <li><a href="#">•••</a></li>
      </c:when>  
      <c:when test="${pNo != current}">
      	<li><a href="javascript:goPage(${pNo});">${pNo}</a></li>
      	<!-- 
        <button class="btn btn-default btn-sm" onclick="pageClick(${pNo})">${pNo}</button> -->
      </c:when>  
      <c:otherwise>
      	<span>${pNo}<span class="sr-only">(current)</span></span>
      	
      	<!-- 
      	<li class="active"><a href="#"></a><span class="sr-only">(current)</span></li>
        <button class="btn btn-primary btn-sm" style="font-weight:bold;">${pNo}</button>-->
      </c:otherwise>  
    </c:choose>
    </c:forTokens>
    
    <c:if test="${page.totalPage != 0 && page.totalPage!=1}"><!-- 只有一页 -->
    <li>
      <a href="javascript:nextPage(${page.currentPage+1});" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
    </c:if>
    
    <li>
      <a href="javascript:goPage(${page.totalPage});" aria-label="Previous">
        <span aria-hidden="true">末页</span>
      </a>
    </li>
  </ul>
</nav>
</form>
<!-- 分页需要的基本参数 -->
<input id="pageCount" type="hidden" value="${page.totalPage}"/>
<input id="path" type="hidden" value="<%=SystemPager.getUrl() %>"/>

<script type="text/javascript">

/*******指哪一页走哪一页********/
function goPage(page){
	$("#currentPage").val(page);
	var path = $("#path").val();
	$("#pager").attr("action",path); 
	$("#pager").submit();
}

/********下一页*********/
function nextPage(page){
	$("#currentPage").val(page);
	var tpage = $("#pageCount").val();
	if(page > tpage){
		return;
	}
	var path = $("#path").val();
	$("#pager").attr("action",path);
	$("#pager").submit();
}

/********上一页*********/
function prePage(page){
	$("#currentPage").val(page);
	if(page==0){
		return;
	}
	var path = $("#path").val();
	$("#pager").attr("action",path); 
	$("#pager").submit();
}
</script>