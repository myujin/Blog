<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="navbar-header">
    <em class="sample-logo"><i class="fa fa-paper-plane"></i></em>
    <span class="navbar-brand">W站后台管理系统</span>
</div>
<!-- 顶端导航栏Start -->
<!-- 消息按钮点击下拉消息列表Start -->
<ul class="nav navbar-top-links navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-envelope fa-fw"></i><i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-messages">
            <li class="divider"></li>
            <li>
                <a class="text-center" href="#">
                    <strong>查看所有消息 </strong>
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </li>
    <!-- 消息按钮点击下拉消息列表End -->
   <!-- 任务按钮点击下拉任务列表Start -->
   <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown" href="#">
           <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
       </a>
       <ul class="dropdown-menu dropdown-tasks">
           <li>
               <a class="text-center" href="#">
                   <strong>查看所有任务 </strong>
                   <i class="fa fa-angle-right"></i>
               </a>
           </li>
       </ul>
   </li>
   <!-- 任务按钮点击下拉任务列表End -->
   <!-- 提醒按钮点击下拉提醒信息列表Start -->
   <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown" href="#">
           <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
       </a>
       <ul class="dropdown-menu dropdown-alerts">
           <li>
               <a class="text-center" href="#">
                   <strong>查看所有提醒 </strong>
                   <i class="fa fa-angle-right"></i>
               </a>
           </li>
       </ul>
   </li>
   <!-- 提醒按钮点击下拉提醒信息列表End -->
   <!-- 用户设置/注销操作下拉菜单Start -->
   <li class="dropdown">
       <a class="dropdown-toggle" data-toggle="dropdown" href="#">
           <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
       </a>
       <ul class="dropdown-menu dropdown-user">
           <li>
               <a href="#" id="alterPassword"><i class="fa fa-unlock-alt"></i> 修改密码</a>
           </li>
           <li>
               <a href="#" id="logout"><i class="fa fa-sign-out fa-fw"></i> 注销</a>
           </li>
       </ul>
   </li>
   <!-- 用户设置/注销操作下拉菜单End -->
</ul>

<!-- /.navbar-static-side -->
