<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<%@include file="/static/common/admin/inc/dwz.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
</head>
<body style="background-color:#fff;">
<div>
    <div class="col-lg-12">
       	<h3 class="page-header">我的质检列表</h3>
        <div class="form-inline well">
            <h4>请选择查询条件</h4>
            <div id="hiddenList">
                <input type="hidden" name="p_modeCondition" value="" />
                <input type="hidden" name="p_mode" value="paperName" />
                <input type="hidden" name="p_discpline" value="" />
                <input type="hidden" name="p_grade" value="" />
                <input type="hidden" name="p_priority" value="" />
                <input type="hidden" id="startNo" value="0" />
            </div>
            <div class="form-group">
                <select id="selectMode" name="selectMode" class="form-control">
                    <option value="" disabled="disbaled">请选择查询类型</option>
                    <option value="paperName">试卷名称</option>
                    <option value="questionCode">试题编号</option>
                    <option value="questionID">试题ID</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" id="modeCondition" name="modeCondition" placeholder="请输入关键字" class="form-control" />
            </div>
            <div class="form-group">
                <select id="stage" name="stage" class="form-control">
                    <option value="-1">选择学段</option>
                    <option>高中</option>
                    <option>高三</option>
                    <option>初中</option>
                    <option>空</option>
                </select>
            </div>
            <div class="form-group">
                <select id="source" name="source" class="form-control">
                    <option value="-1">选择年级</option>
                </select>
            </div>
            <div class="form-group">
                <select id="discpline" name="discpline" class="form-control">
                    <option value="-1">选择学科</option>
                    <option value="1">数学</option>
                    <option value="2">化学</option>
                    <option value="3">物理</option>
                    <option value="4">英语</option>
                    <option value="5">语文</option>
                    <option value="6">生物</option>
                    <option value="7">政治</option>
                    <option value="8">历史</option>
                    <option value="9">地理</option>
                    <option value="0">空</option>
                </select>
            </div>
            <div class="form-group">
                <select id="priority" name="priority" class="form-control">
                    <option value="-1">选择优先级</option>
                    <option value="2">高</option>
                    <option value="1">中</option>
                    <option value="0">低</option>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary btn-long" onclick="selectVideoByPropertity();"><i class="fa fa-search"></i> 查询</button>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr>
                    <th>题号</th>
                    <th>所属试卷</th>
                    <th>学段</th>
                    <th>年级</th>
                    <th>学科</th>
                    <th>录制教师</th>
                    <th>优先级</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2110001649</td>
                    <td>课时变式学与教数学九年级(全一册)九年级下册期末综合027</td>
                    <td>初中</td>
                    <td></td>
                    <td>数学</td>
                    <td>邵泽峰</td>
                    <td>低</td>
                    <td>未质检</td>
                    <td><a class="btn-xs btn-success btn"><i class="fa fa-flag"></i> 质检</a></td>
                </tr>
                <tr>
                    <td>2110001651</td>
                    <td>课时变式学与教数学九年级(全一册)九年级下册期末综合027</td>
                    <td>初中</td>
                    <td></td>
                    <td>数学</td>
                    <td>邵泽峰</td>
                    <td>低</td>
                    <td>未质检</td>
                    <td><a class="btn-xs btn-success btn"><i class="fa fa-flag"></i> 质检</a></td>
                </tr>
                <tr>
                    <td>2110001653</td>
                    <td>课时变式学与教数学九年级(全一册)九年级下册期末综合027</td>
                    <td>初中</td>
                    <td></td>
                    <td>数学</td>
                    <td>邵泽峰</td>
                    <td>低</td>
                    <td>未质检</td>
                    <td><a class="btn-xs btn-success btn"><i class="fa fa-flag"></i> 质检</a></td>
                </tr>
                <tr>
                    <td>2110001817</td>
                    <td>课时变式学与教数学九年级(全一册)九年级下册期末综合027</td>
                    <td>初中</td>
                    <td></td>
                    <td>数学</td>
                    <td>邵泽峰</td>
                    <td>低</td>
                    <td>未质检</td>
                    <td><a class="btn-xs btn-success btn"><i class="fa fa-flag"></i> 质检</a></td>
                </tr>
                <tr>
                    <td>2110001650</td>
                    <td>课时变式学与教数学九年级(全一册)九年级下册期末综合027</td>
                    <td>初中</td>
                    <td></td>
                    <td>数学</td>
                    <td>邵泽峰</td>
                    <td>低</td>
                    <td>未质检</td>
                    <td><a class="btn-xs btn-success btn"><i class="fa fa-flag"></i> 质检</a></td>
                </tr>
            </tbody>
        </table>
        <%@include file="/static/common/admin/common/page.jsp"%>
	</div>
</div>
</body>
</html>