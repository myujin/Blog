<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li>
            <a href="javascript:void(0);"> <b>微众筹系统</b></a>
        </li>
        <!-- ********************************************************************************* -->
        <li>
            <a href="#"><i class="fa fa-sitemap fa-sun-o"></i> 系统管理<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="${path}/admin/sysUser/list.do" target="right"><i class="fa fa-users"></i> 用户管理<span class="fa arrow"></span></a>
                    <%--<a href="${path}/admin/role/roleList" target="right"><i class="fa fa-users"></i> 角色管理<span class="fa arrow"></span></a>
                    <a href="${path}/admin/permission/permissionList" target="right"><i class="fa fa-users"></i> 权限管理<span class="fa arrow"></span></a>--%>
                </li>
            </ul>
        </li>
        <!-- ********************************************************************************* -->
        <li>
            <a href="${path}/admin/user/userList" target="right"><i class="fa fa-users"></i>会员管理 <span class="fa arrow"></span></a>
        </li>
        <!-- ********************************************************************************* -->
        <li>
            <a href="#"><i class="fa fa-book"></i> 项目管理<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="#" onclick="changeSrc('/messageCenter/checkRecord.html')"><i class="fa fa-building-o"></i> 项目设置</a>
                    <a href="#" onclick="changeSrc('/messageCenter/checkRecord.html')"><i class="fa fa-building-o"></i> 退款记录</a>
                    <a href="#" onclick="changeSrc('/messageCenter/checkRecord.html')"><i class="fa fa-building-o"></i> 返款记录</a>
                </li>
            </ul>
        </li>
        <!-- ********************************************************************************* -->
        <li>
            <a href="${path}/admin/notice/noticeList" target="right"><i class="fa fa-commenting fa-fw"></i> 系统公告管理<span class="fa arrow"></span></a>
        </li>
        <!-- ********************************************************************************* -->
        <li>
            <a href="${path}/admin/link/linkList" target="right"><i class="fa fa-columns"></i> 链接管理<span class="fa arrow"></span></a>
        </li>
        <!-- ********************************************************************************* -->
    </ul>
</div>
<!-- /.sidebar-collapse -->
