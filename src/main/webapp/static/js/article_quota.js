/**
 * 文章指标
 * @param type 类型
 * @param articleIds 文章ids
 */
function quota(type, selectorPrefix,articleIds) {
    var url;
    switch(type){
        case "star":
            url = "/quota/stars.do";
            break;
        case "top":
            url = "/quota/tops.do";
            break;
        case "notop":
            url = "/quota/notops.do";
            break;
        case "share":
            url = "/quota/shares.do";
            break;
        case "comment":
            url = "/quota/comments.do";
            break;
    }
    $.ajax({
        url : url,
        type : "GET",
        contentType: "application/json;charset=utf-8",
        data : {'articleIds': articleIds.join(",")},
        dataType : "JSON",
        cache:false,
        async:false,
        success : function(result) {
            if (result.code === 0){
                console.log(result)
                var s = result.data
                for(var articleId in s){
                    $(selectorPrefix+articleId).html(s[articleId])
                }
            }
        },
        error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });
}

function quotaClick(type, selectorPrefix, articleId) {
    var url;
    switch(type){
        case "star":
            url = "/quota/star.do";
            break;
        case "top":
            url = "/quota/top.do";
            break;
        case "notop":
            url = "/quota/noTop.do";
            break;
        case "share":
            url = "/quota/share.do";
            break;
    }
    $.ajax({
        url : url,
        type : "POST",
        data : {'articleId': articleId},
        success : function(result) {
            var res = JSON.parse(result);
            if (res.code === 0) {
                var articleIds = [];
                articleIds.push(articleId);
                quota(type, selectorPrefix, articleIds);
            }
        },
        error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });
}