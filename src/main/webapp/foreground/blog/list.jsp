<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<ul class="w-article-list">
    <c:forEach var="article" items="${articleList}">
	<li class="w-article-detail">
		<input class="articleIds" type="hidden" value="${article.id}"/>
		<div class="article-box article-box-shadow">
			<div class="article-box-title">
				<span class="w-autor-photo">
					<img src="images/u_phote_1.png" width="60px" height="60px"/>
				</span>
				<span class="w-autor-name">
					${article.nickname}
				</span>
				<span class="w-autor-date">
					<i class="icon-date"></i> <fmt:formatDate value="${article.createTime }" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</span>
			</div>
			<div class="article-box-body">
				<h1>
					<p>
						${article.content}
					</p>
				</h1>
			</div>
			<div class="article-box-foot">
				<div class="btn-group">
					<ul>
						<li class="left">
							<span id="top_${article.id}"><i class="icon-zan"></i> </span>
						</li>
						<li class="left">
							<span id="notop_${article.id}"><i class="icon-no-zan"></i> </span>
						</li>
						<li class="left">
							<span id="star_${article.id}"><i class="icon-xx"></i> </span>
						</li>
						<li class="right">
							<span id="share_${article.id}"><i class="icon-share"></i> </span>
						</li>
						<li class="right">
							<span id="comment_${article.id}"><i class="icon-comment"></i> </span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</li>
	</c:forEach>
</ul>
<nav>
	<ul class="x-page">
		${pageCode }
	</ul>
</nav>
<script type="application/javascript">
window.onload = function (){
	var articleIds = []; // 定义一个空数组
    var txt = $(".articleIds"); // 获取所有文本框
    for (var i = 0; i < txt.length; i++) {
        articleIds.push(txt.eq(i).val()); // 将文本框的值添加到数组中
    }
    // 顶
    $.ajax({
        url : "${pageContext.request.contextPath}/quota/tops.do",
		type : "GET",
        contentType: "application/json;charset=utf-8",
        data : {'articleIds': articleIds.join(",")},
		dataType : "JSON",
        cache:false,
        async:false,
        success : function(result) {
            if (result.code === 0){
				console.log(result)
                var s = result.data
                for(var articleId in s){
                    $("#top_"+articleId).append(s[articleId])
                }
            }
        },
		error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });

    // 不顶
    $.ajax({
        url : "${pageContext.request.contextPath}/quota/notops.do",
        type : "GET",
        contentType: "application/json;charset=utf-8",
        data : {'articleIds': articleIds.join(",")},
        dataType : "JSON",
        cache:false,
        async:false,
        success : function(result) {
            if (result.code === 0){
                console.log(result)
                var s = result.data
                for(var articleId in s){
                    $("#notop_"+articleId).append(s[articleId])
                }
            }
        },
        error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });

    // 星星数
    $.ajax({
        url : "${pageContext.request.contextPath}/quota/stars.do",
        type : "GET",
        contentType: "application/json;charset=utf-8",
        data : {'articleIds': articleIds.join(",")},
        dataType : "JSON",
        cache:false,
        async:false,
        success : function(result) {
            if (result.code === 0){
                console.log(result)
                var s = result.data
                for(var articleId in s){
                    $("#star_"+articleId).append(s[articleId])
                }
            }
        },
        error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });

    // 分享数
    $.ajax({
        url : "${pageContext.request.contextPath}/quota/shares.do",
        type : "GET",
        contentType: "application/json;charset=utf-8",
        data : {'articleIds': articleIds.join(",")},
        dataType : "JSON",
        cache:false,
        async:false,
        success : function(result) {
            if (result.code === 0){
                console.log(result)
                var s = result.data
                for(var articleId in s){
                    $("#share_"+articleId).append(s[articleId])
                }
            }
        },
        error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });

    // 评论
    $.ajax({
        url : "${pageContext.request.contextPath}/quota/comments.do",
        type : "GET",
        contentType: "application/json;charset=utf-8",
        data : {'articleIds': articleIds.join(",")},
        dataType : "JSON",
        cache:false,
        async:false,
        success : function(result) {
            if (result.code === 0){
                console.log(result)
                var s = result.data
                for(var articleId in s){
                    $("#comment_"+articleId).append(s[articleId])
                }
            }
        },
        error:function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        }
    });
}
</script>