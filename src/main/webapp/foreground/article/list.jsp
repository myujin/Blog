<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<ul class="w-article-list">
    <c:forEach var="article" items="${articleList}">
	<li class="w-article-detail">
		<input class="articleIds" type="hidden" value="${article.id}"/>
		<div class="article-box article-box-shadow">
			<div class="article-box-title">
				<span class="w-autor-photo">
					<img src="${pageContext.request.contextPath}/static/images/u_phote_1.png" width="60px" height="60px"/>
				</span>
				<span class="w-autor-name">
					${article.nickname}
				</span>
				<span class="w-autor-date">
					<i class="icon-date"></i> <fmt:formatDate value="${article.createTime }" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</span>
			</div>
			<div class="article-box-body">
				<h1>
					<p>
						${article.content}
					</p>
				</h1>
			</div>
			<div class="article-box-foot">
				<div class="btn-group">
					<ul>
						<li class="left">
							<span onclick="javascript:articleTop(${article.id}, ${sessionScope.Wsession.id})"><i class="icon-zan"></i> <e id="top_${article.id}"></e></span>
						</li>
						<li class="left">
							<span onclick="javascript:articleNoTop(${article.id}, ${sessionScope.Wsession.id})"><i class="icon-no-zan"></i> <e id="notop_${article.id}"></e></span>
						</li>
						<li class="left">
							<span onclick="javascript:articleStar(${article.id}, ${sessionScope.Wsession.id})"><i class="icon-xx"></i> <e id="star_${article.id}"></e></span>
						</li>
						<li class="right">
							<span onclick="javascript:articleShare(${article.id}, ${sessionScope.Wsession.id})"><i class="icon-share"></i> <e id="share_${article.id}"></e></span>
						</li>
						<li class="right">
							<a href="${pageContext.request.contextPath}/article/get/${article.id}.html">
							<span><i class="icon-comment"></i> <e id="comment_${article.id}"></e></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</li>
	</c:forEach>
</ul>
<nav>
	<ul class="x-page">
		${pageCode }
	</ul>
</nav>
<script type="application/javascript">
window.onload = function (){
	var articleIds = []; // 定义一个空数组
    var txt = $(".articleIds"); // 获取所有文本框
    for (var i = 0; i < txt.length; i++) {
        articleIds.push(txt.eq(i).val()); // 将文本框的值添加到数组中
    }
    quota("top", "#top_", articleIds);
    quota("notop", "#notop_", articleIds);
    quota("share", "#share_", articleIds);
    quota("star", "#star_", articleIds);
    quota("comment", "#comment_", articleIds);
}
/* 指标数据操作 */
function articleTop(articleId, uid) {
	if (uid === undefined){
	    alert("您还没有登录，请登录！")
	}else {
        quotaClick("top", "#top_", articleId);
    }
}
function articleNoTop(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("notop", "#notop_", articleId);
    }
}
function articleStar(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("star", "#star_", articleId);
    }
}
function articleShare(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("share", "#share_", articleId);
    }
}
</script>