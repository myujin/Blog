<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>W-站 登录</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/common.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/article.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-1.11.2.min.js"></script>
</head>
<body>
<div class="container">
    <jsp:include page="/foreground/common/head.jsp"/>
    <div class="body-wrapper">
        <div class="main">
            <h1>登录</h1>
        </div>
    </div>
    <jsp:include page="/foreground/common/foot.jsp"/>
</div>
</body>
</html>