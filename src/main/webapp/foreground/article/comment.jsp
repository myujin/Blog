<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<ul class="w-article-list w-article-list-shadow">
	<!-- article start -->
	<li class="w-article-detail">
		<input id="w_reply_articleId" type="hidden" value="${articleInfo.id}"/>
		<div class="article-box">
			<div class="article-box-title">
				<span class="w-autor-photo">
					<img src="${pageContext.request.contextPath}/static/images/u_phote_1.png" width="60px" height="60px"/>
				</span>
				<span class="w-autor-name">
					${articleInfo.nickname}
				</span>
				<span class="w-autor-date">
					<i class="icon-date"></i><fmt:formatDate value="${articleInfo.createTime }" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</span>
			</div>
			<div class="article-box-body">
				<h1>
					<p>
						${articleInfo.content}
					</p>
				</h1>
			</div>
			<div class="article-box-foot">
				<div class="btn-group">
					<ul>
						<li class="left">
							<span><i class="icon-zan"></i> <e id="w_article_top_${articleInfo.id}"></e></span>
						</li>
						<li class="left">
							<span><i class="icon-no-zan"></i> <e id="w_article_notop_${articleInfo.id}"></e></span>
						</li>
						<li class="left">
							<span><i class="icon-xx"></i> <e id="w_article_star_${articleInfo.id}"></e></span>
						</li>
						<li class="right">
							<span><i class="icon-share"></i> <e id="w_article_share_${articleInfo.id}"></e></span>
						</li>
						<li class="right">
							<span><i class="icon-comment"></i> <e id="w_article_comment_${articleInfo.id}"></e></span>
						</li>
						<li class="right">
							<span>上一条</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</li>
	<!-- article end -->

	<li class="article-comment">
		<div class="article-comment-list">
			<div class="article-reply article-reply-top-border">
				<div class="w-replyer-photo">
					<span class="w-autor-photo w-reply-autor-pos">
						<img src="${pageContext.request.contextPath}/static/images/u_phote_1.png" width="60px" height="60px"/>
					</span>
					<div class="reply-box">
					</div>

					<div class="reply-cont-num">
						<p>你还可以输入<b>140</b>字</p>
					</div>

					<div class="reply-btn">
						<button type="button">评论</button>
					</div>
				</div>
			</div>
			<ul>
				<!-- reply-list start -->
				<c:if test="${empty commentList}">
					<li class="w-no-reply">
						<span >暂无评论，快来占领沙发~</span>
					</li>
				</c:if>
				<c:if test="${not empty commentList}">
					<c:forEach var="comment" items="${commentList}" varStatus="lou">
						<li class="reply-list article-reply-top-border">
							<div class="w-replyer-photo">
								<div class="reply-box-title">
									<span class="lou-num">${(page.page-1)*page.pageSize + lou.index + 1}楼</span>
									<span class="reply-autor-photo">
								<img src="images/u_phote_1.png" width="60px" height="60px"/>
							</span>

									<span class="reply-a-t">
								<p class="reply-autor-name">${comment.nickname}</p>
								<p class="reply-time"><fmt:formatDate value="${comment.createTime }" type="date" pattern="yyyy-MM-dd HH:mm"/></p>
							</span>

									<div class="reply-btn-group">
										<ul>
											<li>
												<span><i class="icon-zan"></i> 2479</span>
											</li>
											<li>
												<span><i class="icon-no-zan"></i> 2479</span>
											</li>
											<li>
												<span><i class="icon-comment"></i>2479</span>
											</li>
										</ul>
									</div>

								</div>
								<div class="reply-box-bodyer">
									<p>
											${comment.content}
									</p>
								</div>

								<div class="s-reply-num">
									<span><i class="icon-list"></i> 查看短评(0)</span>
								</div>

								<div class="height20"></div>
							</div>
						</li>
					</c:forEach>
				</c:if>
				<!-- reply-list end -->
			</ul>

		</div>
	</li>
</ul>
<nav>
	<ul class="x-page">
		${pageCode }
	</ul>
</nav>
<script src="${pageContext.request.contextPath}/static/js/article_quota.js"></script>
<script type="application/javascript">
window.onload = function (){
    var articleIds = [];
    var articleId = $("#w_reply_articleId").val();
    articleIds.push(articleId);
    quota("top", "#w_article_top_", articleIds);
    quota("notop", "#w_article_notop_", articleIds);
    quota("share", "#w_article_share_", articleIds);
    quota("star", "#w_article_star_", articleIds);
    quota("comment", "#w_article_comment_", articleIds);
}

/* 指标数据操作 */
function articleTop(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("top", "#top_", articleId);
    }
}
function articleNoTop(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("notop", "#notop_", articleId);
    }
}
function articleStar(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("star", "#star_", articleId);
    }
}
function articleShare(articleId, uid) {
    if (uid === undefined){
        alert("您还没有登录，请登录！")
    }else {
        quotaClick("share", "#share_", articleId);
    }
}
</script>