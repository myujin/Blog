<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="w-nrb">
	<div class="w-nrb-t">
		<h2><i class="icon-niu"></i> 牛人榜</h2>
		<span class="w-nrb-label">~大牛们快来围观~</span>
	</div>
	<div class="w-nrb-list">
		<ul>
			<li>
				<div class="nr-detail">
					<span class="nrb-num"> 1</span>
					<span class="nr-phote">
						<img src="${pageContext.request.contextPath}/static/images/u_phote_1.png" width="54px" height="54px"/>
					</span>
					<span class="nr-name"><a href="#">寒蝉鸣泣时</a></span>
					<span class="nr-skill">
						<a>级别：<b class="nr-level">2</b></a>
						<a>节操值：<b class="nr-jc">76</b></a>
					</span>
					<span class="nr-records">这家伙很懒，什么也没留下</span>
				</div>
			</li>

			<li>
				<div class="nr-detail">
					<span class="nrb-num"> 2</span>
					<span class="nr-phote">
						<img src="${pageContext.request.contextPath}/static/mages/u_phote_1.png" width="54px" height="54px"/>
					</span>
					<span class="nr-name"><a href="#">寒蝉鸣泣时</a></span>
					<span class="nr-skill">
						<a>级别：<b class="nr-level">2</b></a>
						<a>节操值：<b class="nr-jc">76</b></a>
					</span>
					<span class="nr-records">这家伙很懒，什么也没留下</span>
				</div>
			</li>

			<li>
				<div class="nr-detail">
					<span class="nrb-num"> 3</span>
					<span class="nr-phote">
						<img src="${pageContext.request.contextPath}/static/images/u_phote_1.png" width="54px" height="54px"/>
					</span>
					<span class="nr-name"><a href="#">寒蝉鸣泣时</a></span>
					<span class="nr-skill">
						<a>级别：<b class="nr-level">2</b></a>
						<a>节操值：<b class="nr-jc">76</b></a>
					</span>
					<span class="nr-records">这家伙很懒，什么也没留下</span>
				</div>
			</li>
		</ul>
	</div>
</div>