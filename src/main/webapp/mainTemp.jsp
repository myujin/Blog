<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>W-站 段子</title>
    <%--<link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap3/css/bootstrap.min.css">--%>
    <%--<link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap3/css/bootstrap-theme.min.css">--%>
    <%--<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/blog.css">--%>
    <%--<link href="http://blog.java1234.com/favicon.ico" rel="SHORTCUT ICON">--%>
    <%--<script src="${pageContext.request.contextPath}/static/bootstrap3/js/jquery-1.11.2.min.js"></script>--%>
    <%--<script src="${pageContext.request.contextPath}/static/bootstrap3/js/bootstrap.min.js"></script>--%>
    <%--<style type="text/css">--%>
    <%--body {--%>
    <%--padding-top: 10px;--%>
    <%--padding-bottom: 40px;--%>
    <%--}--%>
    <%--</style>--%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/common.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/article.css">
    <script src="${pageContext.request.contextPath}/static/js/jquery-1.11.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/article_quota.js"></script>
</head>
<body>
<div class="container">
    <jsp:include page="/foreground/common/head.jsp"/>
    <div class="body-wrapper">
        <div class="main">
            <section class="w-article">
                <jsp:include page="${mainPage }"></jsp:include>
            </section>
            <section class="w-autors">
                <div class="w-nrb">
                    <div class="w-nrb-t">
                        <h2><i class="icon-niu"></i> 牛人榜</h2>
                        <span class="w-nrb-label">~大牛们快来围观~</span>
                    </div>
                    <div class="w-nrb-list">
                        <ul>
                            <li>
                                <div class="nr-detail">
                                    <span class="nrb-num"> 1</span>
                                    <span class="nr-phote">
						<img src="images/u_phote_1.png" width="54px" height="54px"/>
					</span>
                                    <span class="nr-name"><a href="#">寒蝉鸣泣时</a></span>
                                    <span class="nr-skill">
						<a>级别：<b class="nr-level">2</b></a>
						<a>节操值：<b class="nr-jc">76</b></a>
					</span>
                                    <span class="nr-records">这家伙很懒，什么也没留下</span>
                                </div>
                            </li>

                            <li>
                                <div class="nr-detail">
                                    <span class="nrb-num"> 2</span>
                                    <span class="nr-phote">
						<img src="images/u_phote_1.png" width="54px" height="54px"/>
					</span>
                                    <span class="nr-name"><a href="#">寒蝉鸣泣时</a></span>
                                    <span class="nr-skill">
						<a>级别：<b class="nr-level">2</b></a>
						<a>节操值：<b class="nr-jc">76</b></a>
					</span>
                                    <span class="nr-records">这家伙很懒，什么也没留下</span>
                                </div>
                            </li>

                            <li>
                                <div class="nr-detail">
                                    <span class="nrb-num"> 3</span>
                                    <span class="nr-phote">
						<img src="images/u_phote_1.png" width="54px" height="54px"/>
					</span>
                                    <span class="nr-name"><a href="#">寒蝉鸣泣时</a></span>
                                    <span class="nr-skill">
						<a>级别：<b class="nr-level">2</b></a>
						<a>节操值：<b class="nr-jc">76</b></a>
					</span>
                                    <span class="nr-records">这家伙很懒，什么也没留下</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <%--&lt;%&ndash;<jsp:include page="/foreground/common/menu.jsp"/>&ndash;%&gt;--%>
    <%--<div class="row">--%>
    <%--<div class="col-md-9">--%>
    <%--<jsp:include page="${mainPage }"></jsp:include>--%>
    <%--</div>--%>

    <%--<div class="col-md-3">--%>
    <%--<div class="data_list">--%>
    <%--<div class="data_list_title">--%>
    <%--<img src="${pageContext.request.contextPath}/static/images/user_icon.png"/>--%>
    <%--博主信息--%>
    <%--</div>--%>
    <%--<div class="user_image">--%>
    <%--<img src="${pageContext.request.contextPath}/static/userImages/${blogger.imageName }"/>--%>
    <%--</div>--%>
    <%--<div class="nickName">${blogger.nickName }</div>--%>
    <%--<div class="userSign">(${blogger.sign })</div>--%>
    <%--</div>--%>

    <%--<div class="data_list">--%>
    <%--<div class="data_list_title">--%>
    <%--<img src="${pageContext.request.contextPath}/static/images/byType_icon.png"/>--%>
    <%--按日志类别--%>
    <%--</div>--%>
    <%--<div class="datas">--%>
    <%--<ul>--%>
    <%--<c:forEach var="blogTypeCount" items="${blogTypeCountList }">--%>
    <%--<li><span><a--%>
    <%--href="${pageContext.request.contextPath}/index.html?typeId=${blogTypeCount.id }">${blogTypeCount.typeName }(${blogTypeCount.blogCount })</a></span>--%>
    <%--</li>--%>
    <%--</c:forEach>--%>
    <%--</ul>--%>
    <%--</div>--%>
    <%--</div>--%>

    <%--<div class="data_list">--%>
    <%--<div class="data_list_title">--%>
    <%--<img src="${pageContext.request.contextPath}/static/images/byDate_icon.png"/>--%>
    <%--按日志日期--%>
    <%--</div>--%>
    <%--<div class="datas">--%>
    <%--<ul>--%>
    <%--<c:forEach var="blogCount" items="${blogCountList }">--%>
    <%--<li><span><a--%>
    <%--href="index.html?releaseDateStr=${blogCount.releaseDateStr }">${blogCount.releaseDateStr }(${blogCount.blogCount })</a></span>--%>
    <%--</li>--%>
    <%--</c:forEach>--%>
    <%--</ul>--%>
    <%--</div>--%>
    <%--</div>--%>
    <%--</div>--%>


    <%--</div>--%>

    <jsp:include page="/foreground/common/foot.jsp"/>
</div>
</body>
</html>