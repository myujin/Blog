<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>W-站 登录</title>
    <link href="${pageContext.request.contextPath}/static/css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="login_box">
    <div class="login_l_img"><img src="${pageContext.request.contextPath}/static/images/login-img.png" /></div>
    <div class="login">
        <div class="login_logo">
            <a href="#"><img src="${pageContext.request.contextPath}/static/images/login_logo.png" /></a>
        </div>
        <div class="login_name">
            <p>登录</p>
        </div>
        <span class="w-error">${errorInfo}</span>
        <form action="${pageContext.request.contextPath}/login.html" method="post">
            <input name="username" type="text" placeholder="用户名/邮箱..."/>
            <input name="password" type="password" id="password" placeholder="输入密码..." />
            <input value="登录" style="width:100%;" type="submit">
        </form>
        <span class="w-label">亲，如果您还没有账号，请<a href="${pageContext.request.contextPath}/register.html">注册</a></span>
    </div>
    <div class="copyright">Copyright © 2017-2017，京ICP备16037460号</div>
</div>
</body>
</html>
