<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<%@include file="/static/common/admin/inc/dwz.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
</head>
<body style="background-color:#fff;">
<div style="height:150px;"></div>
<div class="row">
	<!-- 
		col-md-offset-4：偏移量
	 -->
  	<div class="col-md-4 col-md-offset-4">
  		<h2 align="center">系统后台管理系统</h2>
  		<form action="${path}/login" method="post">
  			<div style="height:10px;"></div>
  			<div style="height:10px;color:red;">${msg}</div>
  			<div style="height:10px;"></div>
			<div class="form-group">
			  <label for="exampleInputEmail1">用户名</label>
			  <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="用户名..."/>
			</div>
			<div class="form-group">
			  <label for="exampleInputPassword1">密码</label>
			  <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="密码...">
			</div>
			<div style="height:10px;">
			</div>
			<p class="col-md-6 col-md-offset-3">
				<button type="submit" class="btn btn-success"><i class="fa fa-wrench" id="butsubmitCheck" name="butsubmitCheck"></i> 登陆</button>&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="reset" class="btn btn-success"><i class="fa fa-repeat" id="butsubmitCheck" name="butsubmitCheck"></i> 取消</button>
			</p>
		</form>
  	</div>
</div>
</body>
</html>
