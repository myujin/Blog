<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<%@include file="/static/common/admin/inc/dwz.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户列表</title>
</head>
<body style="background-color:#fff;">
	<div >
        <div class="col-lg-12">
            <h3 class="page-header">添加管理员页面</h3>
            <!-- 
            form-inline：排成一条线
            well：外面一个框框
             -->
            <form action="${path}/admin/systemUser/saveSystemUser" class="form-horizontal well" method="post">
                <h4></h4>
                
                <div class="form-group" >
                  <label for="firstname" class="col-sm-2 control-label">真实姓名：</label>
			      <div class="col-sm-10">
			         <input name="name" type="text" class="form-control" id="firstname" placeholder="请输入真实名称..." style="width:300px;"/>
			      </div>
				</div>
				
				<div class="form-group" >
                  <label for="firstname" class="col-sm-2 control-label">登录名称：</label>
			      <div class="col-sm-10">
			         <input name="username" type="text" class="form-control" id="firstname" placeholder="请输入登录名称..." style="width:300px;"/>
			      </div>
				</div>
				
				<div class="form-group" >
                  <label for="firstname" class="col-sm-2 control-label">是否启用：</label>
			      <div class="col-sm-10">
			        	是：<input type="radio" name="status" value="1" checked="checked"/>&nbsp;&nbsp;
			         	否：<input type="radio" name="status" value="0"/>
			      </div>
				</div>
				
				<%-- 
				<div class="form-group" >
                  <label for="firstname" class="col-sm-2 control-label">角色描述：</label>
			      <div class="col-sm-10">
			      	<textarea name="description" class="form-control" style="width:400px;height:200px;"></textarea>
			      </div>
				</div>--%>
				
				<div class="form-group">
				  <label for="firstname" class="col-sm-2 control-label"></label>
			      <div class="col-sm-10">
			        <button type="submit" class="btn btn-success"><i class="fa fa-wrench" id="butsubmitCheck" name="butsubmitCheck"></i> 保存用户</button>&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="reset" class="btn btn-success"><i class="fa fa-repeat" id="butsubmitCheck" name="butsubmitCheck"></i> 取消</button>
			      </div>
            	</div>
            </form>
        </div>
    </div>
</body>
</html>