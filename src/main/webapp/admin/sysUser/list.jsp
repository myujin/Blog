<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<%@include file="/static/common/admin/inc/dwz.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户列表</title>
</head>
<body style="background-color:#fff;">
	<div >
        <div class="col-lg-12">
            <h3 class="page-header">管理员列表</h3>
            <form action="" class="form-inline well" id="fom">
                <div class="form-group">
                    <button id="addAdmin" class="btn btn-info btn-long"><i class="fa fa-plus"></i> 添加管理员</button>
                </div>
            </form>
            <form action="" id="tab" name="tab">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                        	<th>用户昵称</th>
                            <th>登陆名称</th>
                            <th>用户状态</th>
                            <th>登记时间</th>
                            <th>登记人</th>
                            <th width="250px">操作</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                    	<c:if test="${empty adminList}">
                    		<tr>
						  		<td colspan="6" align="center">
									<p class="col-md-4 col-md-offset-4">
						  			<a href="javascript:history.go(-1);" class="btn btn-large btn-danger"><i class="fa fa-arrow-left"></i> 返回上一级</a>
						  			</p>
						  		</td>
						  	</tr>
                    	</c:if>
                    	<c:if test="${!empty adminList}">
                    	<c:forEach items="${adminList}" var="admin">
                        <tr>
                            <td>${admin.nickname}</td>
                            <td>${admin.username}</td>
                            <td>
                            	<c:choose>
                            		<c:when test="${systemUser.status== '1'}">
                            			<span class="label label-success">启用</span>
                            		</c:when>
                            		<c:otherwise>
                            			<span class="btn btn-danger btn-xs">禁用</span>
                            		</c:otherwise>
                            	</c:choose>
                            </td>
                            <td>${admin.inputDate}</td>
                            <td>${admin.inputUser}</td>
                            <td>
                            	<a onclick="javascript:editAdmin(this.name);" class="btn btn-info btn-xs" name="${path}/admin/systemUser/updateSystemUser/${systemUser.id}"><i class="fa fa-pencil"></i> 修改</a>&nbsp;
                            	<a onclick="javascript:deleteAdmin(this.name);" class="btn btn-danger btn-xs" name="${path}/admin/systemUser/deleteSystemUser/${systemUser.id}"><i class="fa fa-trash"></i> 删除</a>
                            	<a onclick="javascript:distributeRole(this.title,this.name);" class="btn btn-success btn-xs" title="${systemUser.id}" name="${systemUser.name}"><i class="fa fa-pencil"></i> 分配角色</a>
                            </td>
                        </tr>
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript">

/*跳转添加页面的*/
$("#addAdmin").on('click', function(){
	$("#fom").attr("action","${path}/admin/sysUser/save.do");
	$("#fom").submit();
});

/*修改用户*/
function editAdmin(path){
	$("#tab").attr("action",path); 
	$("#tab").submit();
}

/*删除用户*/
function deleteAdmin(path){
	$("#tab").attr("action",path); 
	$("#tab").submit();
}
</script>
</html>