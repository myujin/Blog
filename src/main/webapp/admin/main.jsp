<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/static/common/admin/inc/taglib.jsp"%>
<%@include file="/static/common/admin/inc/dwz.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
</head>
<body>
 <!-- <body style="overflow: hidden;"> -->
<div id="wrapper">

	<!-- 导航栏 开始-->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<%@include file="/static/common/admin/common/top.jsp" %>
	</nav>
	<!-- 导航栏结束 -->
	
	<!-- 左侧栏开始 -->
	<div class="sidebar" role="navigation" id="leftNav" style="height:100px;">
		<%@include file="/static/common/admin/common/left.jsp" %>
	</div>
	<!-- 左侧栏结束 -->
	
	<!-- 右侧内容页面开始 -->	
	<div id="page-wrapper">
	 	<iframe style="padding:0px;border:0px;width:100%;height:800px;" src="${pageContext.request.contextPath}/page/common/right.jsp" name="right"></iframe>
	</div>
	<!-- 右侧内容页面结束 -->
</div>
</body>
</html>