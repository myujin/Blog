package com.urwoo.request;

import lombok.Data;

@Data
public class ArticleQueryReq extends BaseQueryReq{
    private Long[] articleCateIds;
    private String title;
    private Integer status;

    public Long[] getArticleCateIds() {
        return articleCateIds;
    }

    public void setArticleCateIds(Long[] articleCateIds) {
        this.articleCateIds = articleCateIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
