package com.urwoo.request;

import lombok.Data;

@Data
public class ArticleCommentQueryReq extends BaseQueryReq{
    private Long articleId;
    private Long userId;
    private Integer status;


    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
