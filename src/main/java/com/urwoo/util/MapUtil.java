package com.urwoo.util;

import java.util.Map;

public class MapUtil {

    public static <K, V> boolean isNull(final Map<K,V> map) {
        return map == null || map.isEmpty();
    }

    public static <K, V> boolean nonNull(final Map<K,V> map){
        return !isNull(map);
    }

}
