package com.urwoo.controller.adapter;

import com.urwoo.entity.ArticleNoTop;
import com.urwoo.entity.ArticleShare;
import com.urwoo.entity.ArticleStar;
import com.urwoo.entity.ArticleTop;
import com.urwoo.util.MapUtil;
import com.urwoo.util.ObjectUtil;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class ArticleQuotaAdapter {


    public static ArticleShare param2Share(Map<String, Object> param){
        ArticleShare articleShare = new ArticleShare();
        if (MapUtil.nonNull(param)){
            articleShare.setArticleId(getArticleId(param));
            articleShare.setUserId(getUserId(param));
            articleShare.setSharePlatform(getSharePlatform(param));
        }
        return articleShare;
    }

    //
    private static Long getUserId(Map<String, Object> param){
        return checkValueNonNull(param, "userId") ?
                Long.parseLong((String)param.get("userId")) : 0L;
    }

    private static Long getArticleId(Map<String, Object> param){
        return checkValueNonNull(param, "article") ?
                Long.parseLong((String)param.get("article")) : 0L;
    }

    private static Integer getSharePlatform(Map<String, Object> param){
        return checkValueNonNull(param, "sharePlatform") ?
                Integer.parseInt((String)param.get("sharePlatform")) : 0;
    }

    protected static boolean checkValueNonNull(Map<String, Object> param, String key) {
        Object obj;
        return ObjectUtil.nonNull(obj = param.get(key)) &&
                StringUtils.isNotBlank((String) obj);
    }
}
