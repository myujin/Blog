package com.urwoo.controller;

import com.urwoo.common.SystemConstants;
import com.urwoo.entity.ArticleInfo;
import com.urwoo.entity.PageBean;
import com.urwoo.entity.User;
import com.urwoo.enums.Status;
import com.urwoo.request.ArticleQueryReq;
import com.urwoo.service.ArticleService;
import com.urwoo.service.UserService;
import com.urwoo.util.Md5Util;
import com.urwoo.util.ObjectUtil;
import com.urwoo.util.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * 主页Controller
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/")
@Slf4j
public class IndexController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private UserService userService;

    /**
     * 请求主页
     */
    @RequestMapping("/index")
    public ModelAndView index(@RequestParam(value = "page", defaultValue = "1") Integer page,
                              @RequestParam(value = "limit", defaultValue = "5") Integer limit,
                              HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView();
        ArticleQueryReq req = new ArticleQueryReq();
        // 启动状态
        req.setStatus(Status.ON.code());
        List<ArticleInfo> articleInfoList = articleService.articles(req, PageBean.start(page, limit), limit);
        long count = articleService.articleCount(req);
        mav.addObject("articleList", articleInfoList);
        // 查询参数
        StringBuffer param = new StringBuffer();
        mav.addObject("pageCode", PageUtil.genPagination(request.getContextPath() + "/index.html",
                count, page, limit, param.toString()));
        mav.addObject("mainPage", "foreground/article/list.jsp");
        mav.setViewName("mainTemp");
        return mav;
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/register")
    public String register() {
        return "register";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        HttpServletRequest request,
                        HttpSession session) {
        User user = userService.getByUsername(username);
        try {

            if (ObjectUtil.nonNull(user)) {
                if (Md5Util.getInstance().validPassword(password, user.getPassword())) {
                    session.setAttribute(SystemConstants.W_SESSION, user);
                    return "redirect:/index.html";
                }
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            log.error("password generate MD5 cause error", e);
        }
        request.setAttribute("errorInfo", "用户名或密码错误！");
        return "login";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpSession session) {
        session.removeAttribute(SystemConstants.W_SESSION);
        return "redirect:/index.html";
    }
}
