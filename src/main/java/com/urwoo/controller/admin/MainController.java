package com.urwoo.controller.admin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/")
@Slf4j
public class MainController {

    @RequestMapping("index")
    public String index() throws Exception {
        return "admin/main";
    }

    @RequestMapping("login")
    public String login(){
        return "admin/login";
    }
}
