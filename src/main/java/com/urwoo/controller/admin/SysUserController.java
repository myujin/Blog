package com.urwoo.controller.admin;

import com.urwoo.common.SystemConstants;
import com.urwoo.entity.SysUser;
import com.urwoo.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
@RequestMapping("/admin/sysUser/")
@Slf4j
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 重定向路径
     */
    private static String REDIRECT_PATH = "redirect:/admin/sysUser/list";

    /**
     * 用户列表
     */
    @RequestMapping("list")
    public String list(Map<String, Object> param, Model model) throws Exception {
//        List<AdminInfo> adminList = sysUserService.();
//        model.addAttribute("adminList",adminList);
        return genSysUserPath("list");
    }

    /**
     * 添加管理员
     */
    @RequestMapping("save")
    public String save() throws Exception {

        return genSysUserPath("add");
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(SysUser sysUser) throws Exception {
        return REDIRECT_PATH;
    }

    /**
     * 修改用户
     */
    @RequestMapping("update/{id}")
    public String update(@PathVariable String id, Model model) throws Exception {

        return genSysUserPath("edit");
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(SysUser sysUser) throws Exception {

        return REDIRECT_PATH;
    }

    /**
     * 删除用户
     */
    @RequestMapping("deleteSystemUser/{id}")
    public String delete(@PathVariable String id) throws Exception {

        return REDIRECT_PATH;
    }


    /**
     * 生成管理员路径
     *
     * @param path
     * @return
     */
    private String genSysUserPath(String path) {
        return SystemConstants.AdminConstant.ADMIN_ROOT_PATH + path;
    }
}
