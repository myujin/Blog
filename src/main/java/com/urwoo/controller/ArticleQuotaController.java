package com.urwoo.controller;

import com.urwoo.controller.responses.WResponses;
import com.urwoo.entity.*;
import com.urwoo.service.ArticleQuotaService;
import com.urwoo.util.JsonUtil;
import com.urwoo.util.ObjectUtil;
import com.urwoo.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/quota/")
public class ArticleQuotaController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(ArticleQuotaController.class);

    @Autowired
    private ArticleQuotaService articleQuotaService;


    @RequestMapping(value = "stars", method = RequestMethod.GET)
    public void starCount(@RequestParam("articleIds") List<Long> articleIds,
                          HttpServletResponse response) {
        WResponses wResponses;
        try {
            Map<String, Long> stars = new HashMap<>();
            articleIds.parallelStream().forEach(id -> {
                long count = articleQuotaService.starCount(id);
                stars.put(String.valueOf(id), count);
            });
            log.info("starCount() : articleIds={}, res={}", articleIds, stars);
            wResponses = WResponses.ok().put("data", stars);

        } catch (Exception e) {
            log.error("get article star num quota cause error", e);
            wResponses = WResponses.error();
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "tops", method = RequestMethod.GET)
    public void topCount(@RequestParam("articleIds") List<Long> articleIds,
                         HttpServletResponse response) {
        WResponses wResponses;
        try {
            Map<String, Long> tops = new HashMap<>();
            articleIds.parallelStream().forEach(id -> {
                long count = articleQuotaService.topCount(id);
                tops.put(String.valueOf(id), count);
            });
            log.info("topCount() : articleIds={}, res={}", articleIds, tops);
            wResponses = WResponses.ok().put("data", tops);
        } catch (Exception e) {
            log.error("get article top num quota cause error", e);
            wResponses = WResponses.error();
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "notops", method = RequestMethod.GET)
    public void noTopCount(@RequestParam("articleIds") List<Long> articleIds,
                           HttpServletResponse response) {
        WResponses wResponses;
        try {
            Map<String, Long> noTops = new HashMap<>();
            articleIds.parallelStream().forEach(id -> {
                long count = articleQuotaService.noTopCount(id);
                noTops.put(String.valueOf(id), count);
            });
            log.info("noTopCount() : articleIds={}, res={}", articleIds, noTops);
            wResponses = WResponses.ok().put("data", noTops);
        } catch (Exception e) {
            log.error("get article noTop num quota cause error", e);
            wResponses = WResponses.error();
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "shares", method = RequestMethod.GET)
    public void shareCount(@RequestParam("articleIds") List<Long> articleIds,
                           HttpServletResponse response) {
        WResponses wResponses;
        try {
            Map<String, Long> shares = new HashMap<>();
            articleIds.parallelStream().forEach(id -> {
                long count = articleQuotaService.shareCount(id);
                shares.put(String.valueOf(id), count);
            });
            log.info("shareCount() : articleIds={}, res={}", articleIds, shares);
            wResponses = WResponses.ok().put("data", shares);
        } catch (Exception e) {
            log.error("get article share num quota cause error", e);
            wResponses = WResponses.error();
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "comments", method = RequestMethod.GET)
    public void commentCount(@RequestParam("articleIds") List<Long> articleIds,
                             HttpServletResponse response) {
        WResponses wResponses;
        try {
            Map<String, Long> comments = new HashMap<>();
            articleIds.parallelStream().forEach(id -> {
                long count = articleQuotaService.commentCount(id);
                comments.put(String.valueOf(id), count);
            });
            log.info("commentCount() : articleIds={}, res={}", articleIds, comments);
            wResponses = WResponses.ok().put("data", comments);
        } catch (Exception e) {
            log.error("get article comment num quota cause error", e);
            wResponses = WResponses.error();
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "star", method = RequestMethod.POST)
    public void star(@RequestParam("articleId") Long articleId,
                     HttpServletResponse response, HttpSession session) {
        WResponses wResponses;
        User user = currentUser(session);
        if (ObjectUtil.isNull(user)) {
            wResponses = WResponses.error(403, "未登录！");
        } else {
            try {
                ArticleStar articleStar = new ArticleStar();
                articleStar.setArticleId(articleId);
                articleStar.setUserId(user.getId());
                articleQuotaService.newStar(articleStar);
                wResponses = WResponses.ok();
            } catch (Exception e) {
                log.error("save article star quota cause error", e);
                wResponses = WResponses.error();
            }
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "top", method = RequestMethod.POST)
    public void top(@RequestParam("articleId") Long articleId,
                    HttpServletResponse response, HttpSession session) {
        WResponses wResponses;
        User user = currentUser(session);
        if (ObjectUtil.isNull(user)) {
            wResponses = WResponses.error(403, "未登录！");
        } else {
            try {
                ArticleTop articleTop = new ArticleTop();
                articleTop.setArticleId(articleId);
                articleTop.setUserId(user.getId());
                articleQuotaService.newTop(articleTop);
                wResponses = WResponses.ok();
            } catch (Exception e) {
                log.error("save article top quota cause error", e);
                wResponses = WResponses.error();
            }
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "noTop", method = RequestMethod.POST)
    public void noTop(@RequestParam("articleId") Long articleId,
                      HttpServletResponse response, HttpSession session) {
        WResponses wResponses;
        User user = currentUser(session);
        if (ObjectUtil.isNull(user)){
            wResponses = WResponses.error(403, "未登录！");
        }else {
            try {
                ArticleNoTop articleNoTop = new ArticleNoTop();
                articleNoTop.setArticleId(articleId);
                articleNoTop.setUserId(user.getId());
                articleQuotaService.newNoTop(articleNoTop);
                wResponses = WResponses.ok();
            } catch (Exception e) {
                log.error("save article noTop quota cause error", e);
                wResponses = WResponses.error();
            }
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }

    @RequestMapping(value = "share", method = RequestMethod.POST)
    public void share(@RequestParam("articleId") Long articleId,
                      @RequestParam("sharePlatform") Integer sharePlatform,
                      HttpServletResponse response, HttpSession session) {
        WResponses wResponses;
        User user = currentUser(session);
        if (ObjectUtil.isNull(user)){
            wResponses = WResponses.error(403, "未登录！");
        }else {
            try {
                ArticleShare articleShare = new ArticleShare();
                articleShare.setArticleId(articleId);
                articleShare.setUserId(user.getId());
                articleShare.setSharePlatform(sharePlatform);
                articleQuotaService.newShare(articleShare);
                wResponses = WResponses.ok();
            } catch (Exception e) {
                log.error("save article share quota cause error", e);
                wResponses = WResponses.error();
            }
        }
        ResponseUtil.write(response, JsonUtil.transformJsonStr(wResponses));
    }
}
