package com.urwoo.controller;

import com.urwoo.common.SystemConstants;
import com.urwoo.entity.User;

import javax.servlet.http.HttpSession;

public class BaseController {

    /**
     * 获取当前用户
     */
    public User currentUser(HttpSession session) {
        return (User) session.getAttribute(SystemConstants.W_SESSION);
    }

}
