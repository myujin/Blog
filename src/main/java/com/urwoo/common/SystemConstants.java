package com.urwoo.common;
/**
 * 系统常量
 * @author yujin
 */
public class SystemConstants {

	public final static String SALT = "www.urwoo.com";

	public final static String W_SESSION = "Wsession";
	
	/**
	 * 时间常量
	 */
	public static class DateContant{
		
		/**
		 * 例子：2015年12月28日11时34分21秒
		 */
		public static String DATE_FORMAT_CN_STR = "yyyy年MM月dd日 HH时mm分ss秒";
		
		/**
		 * 例子：2015-12-28 11:34:21
		 */
		public static String DATE_FORMAT_Y_M_D_H_M_S = "yyyy-MM-dd HH:mm:ss";
		
		/**
		 * 例子：2015-12-28
		 */
		public static String DATE_FORMAT_Y_M_D = "yyyy-MM-dd";
		
		/**
		 * 例子：2015-12-28 11:34
		 */
		public static String DATE_FORMAT_Y_M_D_H_M = "yyyy-MM-dd HH:mm";
		
		/**
		 * 例子：11:34:21
		 */
		public static String DATE_FORMAT_H_M_S = "HH:mm:ss";
	}
	
	/**
	 * 管理员常量
	 */
	public static class AdminConstant{
		public static String ADMIN_ROOT_PATH = "admin/sysUser/";
	}

}
