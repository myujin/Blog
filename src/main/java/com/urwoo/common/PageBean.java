package com.urwoo.common;
import java.util.List;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
/**
 * 封装分页数据,仅仅只适用于mysql
 * @author 余进
 */
public class PageBean<T> implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	public static String DEFAULT_PAGESIZE = "10";
	/**
	 * 当前页码
	 */
	private int currentPage;
	/**
	 * 每页显示的记录数
	 */
	private int pageSize;
	/**
	 * 总记录数
	 */
	private int totalRecord;
	/**
	 * 总页数
	 */
	private int totalPage;
	/**
	 * url地址
	 */
	private String searchUrl;
	/**
	 * 可以显示的页号(分隔符"|"，总页数变更时更新)
	 */
	private String pageNoDisp;
	
	public PageBean(){
		this.pageSize = Integer.valueOf(DEFAULT_PAGESIZE);
		this.currentPage  = 1;
	}
	/**
	 * 总件数变化时，更新总页数并计算显示样式
	 */
	private void refreshPage(){
	    //总页数计算
		totalPage = totalRecord%pageSize==0 ? totalRecord/pageSize : (totalRecord/pageSize + 1);
		//防止超出最末页（浏览途中数据被删除的情况）
	    if ( currentPage > totalPage && totalPage!=0){
	    	currentPage = totalPage;
	    }
	    pageNoDisp = computeDisplayStyleAndPage();
	}

	/**
	 * 计算页号显示样式
	 *  这里实现以下的分页样式("[]"代表当前页号)，可根据项目需求调整
	 * [1],2,3,4,5,6,7,8..12,13
	 * 1,2..5,6,[7],8,9..12,13
	 * 1,2..6,7,8,9,10,11,12,[13]
	 */
	private String computeDisplayStyleAndPage(){
	    List<Integer> pageDisplays = Lists.newArrayList();
	    if ( totalPage <= 11 ){
	      for (int i=1; i<=totalPage; i++){
	        pageDisplays.add(i);
	      }
	    }else if ( currentPage < 7 ){
	      for (int i=1; i<=8; i++){
	        pageDisplays.add(i);
	      }
	      pageDisplays.add(0);// 0 表示 省略部分(下同)
		  pageDisplays.add(totalPage-1);       
		  pageDisplays.add(totalPage);
		}else if ( currentPage> totalPage-6 ){
		  pageDisplays.add(1);
		  pageDisplays.add(2);
		  pageDisplays.add(0);
		  for (int i=totalPage-7; i<=totalPage; i++){
		    pageDisplays.add(i);
		  }       
		}else{
		  pageDisplays.add(1);
		  pageDisplays.add(2);
		  pageDisplays.add(0);
		  for (int i=currentPage-2; i<=currentPage+2; i++){
		  	pageDisplays.add(i);
		  }
		  pageDisplays.add(0);
		  pageDisplays.add(totalPage-1);
		  pageDisplays.add(totalPage);
		}
		return Joiner.on("|").join(pageDisplays.toArray());
	}

	/**********************setter，getter方法************************/
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
	    return pageSize;
	}
	public void setPageSize(int pageSize) {
	    this.pageSize = pageSize;
	}
	public int getTotalRecord() {
	    return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
	    this.totalRecord = totalRecord;
	    refreshPage();     
	}
	public int getTotalPage() {
	    return totalPage;
	}
	public void setTotalPage(int totalPage) {
	    this.totalPage = totalPage;
	}
	public String getSearchUrl() {
	    return searchUrl;
	}
	public void setSearchUrl(String searchUrl) {
	    this.searchUrl = searchUrl;
	}
	public String getPageNoDisp() {
	    return pageNoDisp;
	}
	public void setPageNoDisp(String pageNoDisp) {
	    this.pageNoDisp = pageNoDisp;
	}
}