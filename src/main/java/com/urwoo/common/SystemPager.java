package com.urwoo.common;


/**
 * 分页内容
 * @author Administrator
 */
public class SystemPager {
	
	/**
	 * 当前页
	 */
	private static ThreadLocal<Integer>  currentpage = new ThreadLocal<Integer>();
	
	/**
	 * 分页大小
	 */
	private static ThreadLocal<Integer> pagesize = new ThreadLocal<Integer>();
	
	/**
	 * 跳转的url
	 */
	private static ThreadLocal<String> url = new ThreadLocal<String>();
	
	public static Integer getCurrentpage() {
		return currentpage.get();
	}

	public static void setCurrentpage(Integer _currentpage) {
		SystemPager.currentpage.set(_currentpage);
	}

	public static Integer getPagesize() {
		return pagesize.get();
	}

	public static void setPagesize(Integer _pagesize) {
		SystemPager.pagesize.set(_pagesize);
	}


	public static String getUrl() {
		return url.get();
	}

	public static void setUrl(String _url) {
		SystemPager.url.set(_url);
	}
	
	public static void removeCurrentPage(){
		currentpage.remove();
	}
	
	public static void removePageSize(){
		pagesize.remove();
	}
	
	public static void removeUrl(){
		url.remove();
	}
}
