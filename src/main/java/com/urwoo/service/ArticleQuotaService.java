package com.urwoo.service;

import com.urwoo.entity.ArticleNoTop;
import com.urwoo.entity.ArticleShare;
import com.urwoo.entity.ArticleStar;
import com.urwoo.entity.ArticleTop;

public interface ArticleQuotaService {

    long starCount(Long articleId);

    long topCount(Long articleId);

    long noTopCount(Long articleId);

    long shareCount(Long articleId);

    long commentCount(Long articleId);

    void newStar(ArticleStar articleStar);

    void newTop(ArticleTop articleTop);

    void newNoTop(ArticleNoTop articleNoTop);

    void newShare(ArticleShare articleShare);
}
