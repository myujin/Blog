package com.urwoo.service;

import com.urwoo.entity.SysUser;
import com.urwoo.request.SysUserQueryReq;

import java.util.List;

public interface SysUserService {
    int save(SysUser sysUser);

    int update(SysUser sysUser);

    int batchUpdateStatus(List<Long> ids, Integer status);

    SysUser getById(Long id);

    SysUser getByUsername(String username);

    List<SysUser> query(SysUserQueryReq param, Long start, Integer limit);

    Long count(SysUserQueryReq param);
}
