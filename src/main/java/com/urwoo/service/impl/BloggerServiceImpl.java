package com.urwoo.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.urwoo.dao.BloggerDao;
import com.urwoo.entity.Blogger;
import com.urwoo.service.BloggerService;

/**
 * 博主Service实现类
 * @author Administrator
 *
 */
@Service("bloggerService")
public class BloggerServiceImpl implements BloggerService{

	@Resource
	private BloggerDao bloggerDao;

	@Override
	public Blogger find() {
		return bloggerDao.find();
	}

	@Override
	public Blogger getByUserName(String userName) {
		return bloggerDao.getByUserName(userName);
	}

	@Override
	public Integer update(Blogger blogger) {
		return bloggerDao.update(blogger);
	}
	
	
}
