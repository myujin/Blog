package com.urwoo.service.impl;

import com.urwoo.dao.*;
import com.urwoo.entity.ArticleNoTop;
import com.urwoo.entity.ArticleShare;
import com.urwoo.entity.ArticleStar;
import com.urwoo.entity.ArticleTop;
import com.urwoo.enums.Status;
import com.urwoo.request.*;
import com.urwoo.service.ArticleQuotaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@Slf4j
public class ArticleQuotaServiceImpl implements ArticleQuotaService {

    @Autowired
    private ArticleTopDao articleTopDao;
    @Autowired
    private ArticleNoTopDao articleNoTopDao;
    @Autowired
    private ArticleShareDao articleShareDao;
    @Autowired
    private ArticleStarDao articleStarDao;
    @Autowired
    private ArticleCommentDao articleCommentDao;

    public long starCount(Long articleId) {
        ArticleStarQueryReq req = new ArticleStarQueryReq();
        req.setArticleId(articleId);
        req.setStatus(Status.ON.code());
        long count = articleStarDao.count(req);
        log.info("starCount() : articleId={}, count={}", articleId, count);
        return count;
    }

    public long topCount(Long articleId) {
        ArticleTopQueryReq req = new ArticleTopQueryReq();
        req.setArticleId(articleId);
        req.setStatus(Status.ON.code());
        long count = articleTopDao.count(req);
        log.info("topCount() : articleId={}, count={}", articleId, count);
        return count;
    }

    public long noTopCount(Long articleId) {
        ArticleNoTopQueryReq req = new ArticleNoTopQueryReq();
        req.setArticleId(articleId);
        req.setStatus(Status.ON.code());
        long count = articleNoTopDao.count(req);
        log.info("noTopCount() : articleId={}, count={}", articleId, count);
        return count;
    }

    public long shareCount(Long articleId) {
        ArticleShareQueryReq req = new ArticleShareQueryReq();
        req.setArticleId(articleId);
        req.setStatus(Status.ON.code());
        long count = articleShareDao.count(req);
        log.info("shareCount() : articleId={}, count={}", articleId, count);
        return count;
    }

    public long commentCount(Long articleId) {
        ArticleCommentQueryReq req = new ArticleCommentQueryReq();
        req.setArticleId(articleId);
        req.setStatus(Status.ON.code());
        long count = articleCommentDao.count(req);
        log.info("commentCount() : articleId={}, count={}", articleId, count);
        return count;
    }

    public void newStar(ArticleStar articleStar) {

        Assert.notNull(articleStar.getArticleId(), "article id must not be null!");
        Assert.notNull(articleStar.getUserId(), "user id must not be null!");
        log.info("star() : articleStar={}", articleStar.toString());

        articleStar.setStatus(Status.ON.code());
        articleStarDao.save(articleStar);
    }

    public void newTop(ArticleTop articleTop) {

        Assert.notNull(articleTop.getArticleId(), "article id must not be null!");
        Assert.notNull(articleTop.getUserId(), "user id must not be null!");
        log.info("top() : articleTop={}", articleTop.toString());

        articleTop.setStatus(Status.ON.code());
        articleTopDao.save(articleTop);
    }

    public void newNoTop(ArticleNoTop articleNoTop) {

        Assert.notNull(articleNoTop.getArticleId(), "article id must not be null!");
        Assert.notNull(articleNoTop.getUserId(), "user id must not be null!");
        log.info("noTop() : articleNoTop={}", articleNoTop.toString());
        articleNoTop.setStatus(Status.ON.code());
        articleNoTopDao.save(articleNoTop);
    }

    public void newShare(ArticleShare articleShare) {

        Assert.notNull(articleShare.getArticleId(), "article id must not be null!");
        Assert.notNull(articleShare.getUserId(), "user id must not be null!");
        Assert.notNull(articleShare.getSharePlatform(), "share platform must not be null!");
        log.info("share() : articleShare={}", articleShare.toString());
        articleShare.setStatus(Status.ON.code());
        articleShareDao.save(articleShare);
    }
}
