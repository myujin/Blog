package com.urwoo.service.impl;

import com.urwoo.dao.SysUserMapper;
import com.urwoo.entity.SysUser;
import com.urwoo.request.SysUserQueryReq;
import com.urwoo.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService{

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public int save(SysUser sysUser) {
        return sysUserMapper.save(sysUser);
    }

    @Override
    public int update(SysUser sysUser) {
        return sysUserMapper.update(sysUser);
    }

    @Override
    public int batchUpdateStatus(List<Long> ids, Integer status) {
        return sysUserMapper.batchUpdateStatus(ids, status);
    }

    @Override
    public SysUser getById(Long id) {
        return sysUserMapper.getById(id);
    }

    @Override
    public SysUser getByUsername(String username) {
        return sysUserMapper.getByUsername(username);
    }

    @Override
    public List<SysUser> query(SysUserQueryReq param, Long start, Integer limit) {
        return sysUserMapper.query(param, start, limit);
    }

    @Override
    public Long count(SysUserQueryReq param) {
        return sysUserMapper.count(param);
    }
}
