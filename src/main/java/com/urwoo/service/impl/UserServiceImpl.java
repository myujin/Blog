package com.urwoo.service.impl;

import com.urwoo.dao.UserDao;
import com.urwoo.entity.User;
import com.urwoo.enums.Status;
import com.urwoo.request.UserQueryReq;
import com.urwoo.service.UserService;
import com.urwoo.util.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;

    @Override
    public int save(User user) {
        return userDao.save(user);
    }

    @Override
    public int update(User user) {
        return userDao.update(user);
    }

    @Override
    public int updatePwd(Long id, String password) {
        return userDao.updatePwd(id, password);
    }

    @Override
    public int batchUpdateStatus(List<Long> ids, Integer status) {
        return 0;
    }

    @Override
    public User getById(Long id) {
        return null;
    }

    @Override
    public User getByUsername(String username) {
        User user = userDao.getByUsername(username);
        if (ObjectUtil.isNull(user) ||
                ObjectUtil.equals(user.getStatus(), Status.DEL.code())){
            return null;
        }
        return user;
    }

    @Override
    public User getByPhone(String phone) {
        return null;
    }

    @Override
    public User getByEmail(String email) {
        return null;
    }

    @Override
    public List<User> query(UserQueryReq param, Integer start, Integer limit) {
        return null;
    }

    @Override
    public Long count(UserQueryReq param) {
        return null;
    }
}
