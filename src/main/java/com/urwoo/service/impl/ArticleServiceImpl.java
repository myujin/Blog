package com.urwoo.service.impl;

import com.urwoo.dao.ArticleDao;
import com.urwoo.entity.ArticleInfo;
import com.urwoo.request.ArticleQueryReq;
import com.urwoo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService{

    @Autowired
    private ArticleDao articleDao;

    @Override
    public List<ArticleInfo> articles(ArticleQueryReq req, int start, int limit) {
        return articleDao.query(req, start, limit);
    }

    @Override
    public long articleCount(ArticleQueryReq req) {
        return articleDao.count(req);
    }

    @Override
    public ArticleInfo getInfo(Long id) {
        return articleDao.getInfo(id);
    }
}
