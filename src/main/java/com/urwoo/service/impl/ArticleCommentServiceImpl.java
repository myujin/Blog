package com.urwoo.service.impl;

import com.urwoo.dao.ArticleCommentDao;
import com.urwoo.entity.ArticleCommentInfo;
import com.urwoo.request.ArticleCommentQueryReq;
import com.urwoo.service.ArticleCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleCommentServiceImpl implements ArticleCommentService {

    @Autowired
    private ArticleCommentDao articleCommentDao;

    @Override
    public List<ArticleCommentInfo> comments(ArticleCommentQueryReq param, Integer start, Integer limit) {
        return articleCommentDao.query(param, start, limit);
    }

    @Override
    public Long count(ArticleCommentQueryReq param) {
        return articleCommentDao.count(param);
    }
}
