package com.urwoo.service;

import com.urwoo.entity.User;
import com.urwoo.request.UserQueryReq;

import java.util.List;

public interface UserService {

    int save(User user);

    int update(User user);

    int updatePwd(Long id, String password);

    int batchUpdateStatus(List<Long> ids, Integer status);

    User getById(Long id);

    User getByUsername(String username);

    User getByPhone(String phone);

    User getByEmail(String email);

    List<User> query(UserQueryReq param, Integer start, Integer limit);

    Long count(UserQueryReq param);

}
