package com.urwoo.service;

import com.urwoo.entity.ArticleCommentInfo;
import com.urwoo.request.ArticleCommentQueryReq;

import java.util.List;

public interface ArticleCommentService {

    List<ArticleCommentInfo> comments(ArticleCommentQueryReq param,
                                   Integer start,
                                   Integer limit);

    Long count(ArticleCommentQueryReq param);
}
