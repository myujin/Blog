package com.urwoo.service;

import com.urwoo.entity.ArticleInfo;
import com.urwoo.request.ArticleQueryReq;

import java.util.List;

public interface ArticleService {

    List<ArticleInfo> articles(ArticleQueryReq req, int start, int limit);

    long articleCount(ArticleQueryReq articleQueryReq);

    ArticleInfo getInfo(Long id);
}
