package com.urwoo.dao;

import com.urwoo.entity.ArticleNoTop;
import com.urwoo.request.ArticleNoTopQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleNoTopDao {

    int save(ArticleNoTop articleNoTop);


    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);


    ArticleNoTop getById(@Param("id") Long id);


    Long count(@Param("param") ArticleNoTopQueryReq param);
}
