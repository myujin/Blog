package com.urwoo.dao;

import com.urwoo.entity.Blogger;

/**
 * ����Dao�ӿ�
 * @author java1234_С��
 *
 */
public interface BloggerDao {

	/**
	 * ��ѯ������Ϣ
	 * @return
	 */
	Blogger find();
	
	/**
	 * ͨ���û�����ѯ�û�
	 * @param userName
	 * @return
	 */
	Blogger getByUserName(String userName);
	
	/**
	 * ���²�����Ϣ
	 * @param blogger
	 * @return
	 */
	Integer update(Blogger blogger);
}
