package com.urwoo.dao;

import com.urwoo.entity.ArticleStar;
import com.urwoo.request.ArticleStarQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleStarDao {

    int save(ArticleStar articleStar);


    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);


    ArticleStar getById(@Param("id") Long id);


    Long count(@Param("param") ArticleStarQueryReq param);
}
