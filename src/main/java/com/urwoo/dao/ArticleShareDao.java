package com.urwoo.dao;

import com.urwoo.entity.ArticleShare;
import com.urwoo.request.ArticleShareQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleShareDao {

    int save(ArticleShare articleShare);


    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);


    ArticleShare getById(@Param("id") Long id);


    Long count(@Param("param") ArticleShareQueryReq param);
}
