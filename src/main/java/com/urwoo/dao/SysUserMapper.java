package com.urwoo.dao;

import com.urwoo.entity.SysUser;
import com.urwoo.request.SysUserQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {

    int save(SysUser sysUser);

    int update(SysUser sysUser);

    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);

    SysUser getById(@Param("id") Long id);

    SysUser getByUsername(@Param("username") String username);

    List<SysUser> query(@Param("param") SysUserQueryReq param,
                        @Param("start") Long start,
                        @Param("limit") Integer limit);

    Long count(@Param("param") SysUserQueryReq param);
}
