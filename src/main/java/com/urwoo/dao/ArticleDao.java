package com.urwoo.dao;

import com.urwoo.entity.Article;
import com.urwoo.entity.ArticleInfo;
import com.urwoo.request.ArticleQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleDao {

    int save(Article article);

    int update(Article article);

    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);

    int batchUpdateCheckStatus(@Param("ids") List<Long> ids,
                               @Param("checkStatus") Integer checkStatus);

    Article getById(@Param("id") Long id);

    Article getByTitle(@Param("userId") Long userId, @Param("title") String title);

    List<ArticleInfo> query(@Param("param") ArticleQueryReq param,
                              @Param("start") Integer start,
                              @Param("limit") Integer limit);

    ArticleInfo getInfo(@Param("id") Long id);

    Long count(@Param("param") ArticleQueryReq param);
}
