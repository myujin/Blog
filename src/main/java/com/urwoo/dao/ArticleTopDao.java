package com.urwoo.dao;

import com.urwoo.entity.ArticleTop;
import com.urwoo.request.ArticleTopQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleTopDao {

    int save(ArticleTop articleTop);


    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);


    ArticleTop getById(@Param("id") Long id);


    Long count(@Param("param") ArticleTopQueryReq param);
}
