package com.urwoo.dao;

import com.urwoo.entity.ArticleComment;
import com.urwoo.entity.ArticleCommentInfo;
import com.urwoo.request.ArticleCommentQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleCommentDao {

    int save(ArticleComment articleComment);

    int update(ArticleComment articleComment);

    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);

    ArticleComment getById(@Param("id") Long id);

    List<ArticleCommentInfo> query(@Param("param") ArticleCommentQueryReq param,
                               @Param("start") Integer start,
                               @Param("limit") Integer limit);

    Long count(@Param("param") ArticleCommentQueryReq param);
}
