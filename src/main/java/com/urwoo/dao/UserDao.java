package com.urwoo.dao;

import com.urwoo.entity.User;
import com.urwoo.request.UserQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    int save(User user);

    int update(User user);

    int updatePwd(@Param("id") Long id, @Param("password") String password);

    int batchUpdateStatus(@Param("ids") List<Long> ids, @Param("status") Integer status);

    User getById(@Param("id") Long id);

    User getByUsername(@Param("username") String username);

    User getByPhone(@Param("phone") String phone);

    User getByEmail(@Param("email") String email);

    List<User> query(@Param("param") UserQueryReq param,
                     @Param("start") Integer start,
                     @Param("limit") Integer limit);

    Long count(@Param("param") UserQueryReq param);

}
