package com.urwoo.dao;

import com.urwoo.entity.ArticleCategory;
import com.urwoo.request.ArticleCategoryQueryReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleCategoryDao {

    int save(ArticleCategory articleCategory);

    int update(ArticleCategory articleCategory);

    int batchUpdateStatus(@Param("ids") List<Long> ids,
                          @Param("status") Integer status);

    ArticleCategory getById(@Param("id") Long id);

    ArticleCategory getByName(@Param("name") String name);

    List<ArticleCategory> query(@Param("param") ArticleCategoryQueryReq param,
                                  @Param("start") Long start,
                                  @Param("limit") Integer limit);

    List<ArticleCategory> listByStatus(@Param("status") Integer status);

    Long count(@Param("param") ArticleCategoryQueryReq param);
}
