package com.urwoo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class SysUser {
    private Long id;
    private String name;
    private String username;
    private String password;
    private String ssoId;
    private Integer status;
    private Date createTime;
    private Date modifyTime;
}
