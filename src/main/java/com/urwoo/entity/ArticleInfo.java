package com.urwoo.entity;

import lombok.Data;

@Data
public class ArticleInfo extends Article {
    private String username;
    private String nickname;
    private String articleCategoryName;
}
