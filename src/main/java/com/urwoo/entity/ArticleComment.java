package com.urwoo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleComment {
    protected Long id;
    protected Long userId;
    protected Long articleId;
    protected String content;
    protected Integer status;
    protected Date createTime;
    protected Date modifyTime;
}
