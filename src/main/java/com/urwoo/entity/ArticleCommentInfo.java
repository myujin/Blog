package com.urwoo.entity;

import lombok.Data;

@Data
public class ArticleCommentInfo extends ArticleComment{
    private String username;
    private String nickname;
}
